import PropTypes from 'prop-types'
import Head from 'next/head'
import Link from 'next/link'

const propTypes = {
  name: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node
  ]).isRequired
}

function PageTemplate(props) {
 return ( 
   <>
    <Head>
      <title>Mii Makeup | {props.name}</title>
      <meta name="description" content={`Mii makeup ${props.name} page`} />
      <link rel="icon" href="/favicon.ico" />
      <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet" />
    </Head>

    <section className="flex flex-col text-gray-600 h-screen font-sans">
      <nav className="flex bg-gradient-to-br from-pink-500 to-rose-500 text-white font-bold">
        <Link href="/">
          <a className="p-4 hover:bg-pink-600">
            Home
          </a>
        </Link>

        <Link href="/search">
          <a className="p-4 hover:bg-pink-600">
            Search
          </a>
        </Link>
      </nav>

      <main className="flex-grow flex flex-col items-center">
        {props.children}
      </main>

      <footer className="flex justify-center pt-32 pb-16 text-gray-600 text-sm">
        <a
          href="https://www.linkedin.com/in/nicoandresr/"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '} NicoandresR
        </a>
      </footer>
    </section>
   </>
  )
}

PageTemplate.propTypes = propTypes

export default PageTemplate
