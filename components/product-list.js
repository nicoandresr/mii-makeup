import PropTypes from 'prop-types'

import Product from './product'

const propTypes = {
  products: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number
  }))
}

const defaultProps = {
}

function ProductList(props) {
  return (
    <div className="flex flex-wrap container">
      {
        // Here we can implement a infinite scroll feature
        // for load products on demand and no load all the data
        // to avoid performance issues
        // here we only load the first 10 products by default by
        // the Data API
        // <InfiniteScrollWithPagination>
      }
      {
        props.products.map(({ id, ...rest }) => (
          <Product key={id} id={id} {...rest} />
        ))
      }
      {
        // </InfiniteScrollWithPagination>
      }
    </div>
  )
}

ProductList.propTypes = propTypes
ProductList.defaultProps = defaultProps

export default ProductList
