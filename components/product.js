import PropTypes from 'prop-types'
import Link from 'next/link'

const propTypes = {
  id: PropTypes.number.isRequired,
  image_link: PropTypes.string,
  name: PropTypes.string,
  api_featured_image: PropTypes.string,
  rating: PropTypes.string,
  price: PropTypes.string,
  price_sign: PropTypes.string,
}

const defaultProps = {
  image_link: '',
  name: '',
  api_featured_image: '',
}

function Product(props) {
  return (
    <Link href={`/product/${props.id}`}>
      <figure className="flex bg-gray-100 rounded-xl shadow-xl h-32 w-96 m-4 cursor-pointer hover:bg-gray-200">
        <picture className="">
          <source srcSet={props.api_featured_image} media="(min-width: 600px)" />

          <img className="h-full w-32 object-cover rounded-l-xl" src={props.api_featured_image} alt={props.name} loading="lazy" />
        </picture>

        <figcaption className="text-xl m-4 w-full flex flex-col">
          <div className="font-semibold">
            {props.name}
          </div>

          <div className="text-sm">
            {props.rating && `Rating: ${props.rating}`}
          </div>


          <div className="text-3xl m-auto text-blue-300 font-semibold">
            {
              props.price !== '0.0'
                ? `${props.price_sign ? `${props.price_sign} ` : ''}${props.price}`
                : 'Sold-Out'
            }
          </div>
        </figcaption>
      </figure>
    </Link>
  )
}

Product.propTypes = propTypes
Product.defaultProps = defaultProps

export default Product
