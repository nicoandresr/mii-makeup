import dataApi from '../../../lib/data-api'

export default async function handler(req, res) {
  let api = await dataApi()
  let product = api.productById(+req.query.id)

  res.status(200).json(product)
}
