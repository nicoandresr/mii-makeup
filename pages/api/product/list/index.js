import dataApi from '../../../../lib/data-api'

export default async (req, res) => {
  let pageNumber = +req.query.pageNumber || 1
  let pageSize = +req.query.pageSize || 10
  let pageIndex = pageNumber - 1

  let api = await dataApi()

  const pageItems = api.products.slice(pageIndex * pageSize, pageNumber * pageSize)
  res.status(200).json(pageItems)
}
