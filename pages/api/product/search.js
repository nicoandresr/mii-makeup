import dataApi from '../../../lib/data-api'

export default async function handler(req, res) {
  let api = await dataApi()

  let {
    name,
    type,
    priceLess,
    priceGreater,
    ratingLess,
    ratingGreater,
    brand,
  } = req.query

  let result = []

  if (name && name.length > 2)
    result = api.products.filter(product => new RegExp(name, 'ig').test(product.name))

  if (type)
    result = result.filter(product => product.product_type === type.toLowerCase())

  if (brand)
    result = result.filter(product => product.brand === brand.toLowerCase())

  if (ratingGreater)
    result = result.filter(product => product.rating >= +ratingGreater)

  if (ratingLess)
    result = result.filter(product => product.rating <= +ratingLess)

  if (priceGreater)
    result = result.filter(product => +product.price >= +priceGreater)

  if (priceLess)
    result = result.filter(product => +product.price <= +priceLess)

  res.status(200).json(result)
}
