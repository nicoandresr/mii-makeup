import * as React from 'react'
import PropTypes from 'prop-types'

import PageTemplate from '../components/page-template'
import ProductList from '../components/product-list'
import host from '../lib/host'

const propTypes = {
  products: PropTypes.array
}

const defaultProps = {
  products: []
}

export async function getStaticProps() {
  const response = await fetch(`${host()}/products.json`)
  const products = await response.json()
  return {
    props: { products: products.slice(0, 20) }
  }
}

function Home({ products }) {
  const [result, setResult] = React.useState([])

  function onSubmitHandler(event) {
    event.preventDefault()
    let [{ value }] = event.target 
    setResult(products.filter(product =>
      new RegExp(value, 'i').test(product.name)
    ))
  }

  return (
    <PageTemplate name="Search">
      <div className="container flex flex-col items-center">
        <h1 className="text-4xl p-4">
          Search products
        </h1>

        <form
          className="w-full"
          onSubmit={onSubmitHandler}
        >
          <div className="flex w-full max-w-sm mx-auto space-x-3">
            <input
              className="flex-1 appearance-none border border-transparent w-80 py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-md rounded-lg text-base focus:outline-none focus:ring-2 focus:ring-pink-600 focus:border-transparent"
              type="text"
              id="name"
            />

            <button
              className="flex-shrink-0 bg-pink-600 text-white text-base font-semibold py-2 px-4 rounded-lg shadow-md hover:bg-pink-700 focus:outline-none focus:ring-2 focus:ring-pink-500 focus:ring-offset-2 focus:ring-offset-pink-200"
              type="submit"
            >
              Search
            </button>
          </div>

          <details className="cursor-pointer mt-8 mb-8">
            <summary>
              Advanced search
            </summary>
            ... here we can implement advanced search by type, price, rating, etc, the local api supports this search but for time limitation, this advanced search is not implemented
          </details>

        </form>

        <div>
          {!result && <h2>Loading...</h2>}
          {
            // Here we can implement a infinite scroll feature
            // for load products on demand and no load all the data
            // to avoid performance issues
            // here we only load the first 10 products by default by
            // the Data API
            // <InfiniteScrollWithPagination>
          }
          {result && result.length > 0 && (
            <>
              <div className="text-sm text-semibold">
                Tolal results: {result.length}
              </div>

              <ProductList products={result} />
            </>
          )}
          {
            // </InfiniteScrollWithPagination>
          }
        </div>
      </div>
    </PageTemplate>
  )
}

Home.propTypes = propTypes
Home.defaultProps = defaultProps

export default Home
