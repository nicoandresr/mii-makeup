import PropTypes from 'prop-types'

import PageTemplate from '../../components/page-template'
import host from '../../lib/host'

const propTypes = {
  product: PropTypes.shape({
    name: PropTypes.string,
    image_link: PropTypes.string,
    api_featured_image: PropTypes.string,
    description: PropTypes.string,
    product_colors: PropTypes.arrayOf(PropTypes.shape({
      hex_value: PropTypes.string,
      colour_name: PropTypes.string,
    })),
    price: PropTypes.string,
    price_sign: PropTypes.string,
    rating: PropTypes.number,
  })
}

const defaultProps = {
}

export async function getStaticProps(context) {
  const { id } = context.params
  const response = await fetch(`${host()}/products.json`)
  const products = await response.json()
  const [product] = products.slice(0, 20).filter(product => product.id === +id)
  return {
    props: { product }
  }
}

export async function getStaticPaths() { 
  const response = await fetch(`${host()}/products.json`)
  const products = await response.json()

  const paths = products.slice(0, 20).map(product => ({
    params: { id: `${product.id}` },
  }))

  return {
    paths,
    fallback: false
  }
}

function ProductDetail({ product }) {
  return (
    <PageTemplate name="Detail">
      <div className="flex flex-col items-center container">
        <h1 className="text-4xl p-4">
          {product.name}
        </h1>

        <div className="flex">
          <picture>
            <source srcSet={product.image_link} type="image/jpeg" />

            <img className="rounded-xl shadow-xl object-cover m-2"
              width="300px" src={product.api_featured_image} alt={product.name} loading="lazy"
            />
          </picture>

          <picture>
            <source srcSet={product.api_featured_image} type="image/jpeg" />

            <img className="rounded-xl shadow-xl object-cover m-2"
              width="300px" src={product.api_featured_image} alt={product.name} loading="lazy" 
            />
          </picture>
        </div>

        <dl className="w-1/2">
          <dt className="mt-8 text-xl font-semibold">
            Description
          </dt>
          <dd>
            <p>{product.description}</p>
          </dd>

          <dt className="mt-4 text-md font-semibold">
            Colors
          </dt>
          <dd>
            <ul className="grid grid-flow-row grid-cols-3 gap-2 m-4">
              {product.product_colors.map(color => (
                <li
                  className="relative"
                  key={color.hex_value}
                  title={color.colour_name}
                >
                  {color.colour_name}
                  <span
                    className="absolute right-4 rounded-full p-3 w-10"
                    style={{ backgroundColor: color.hex_value }}
                  />
                </li>
              ))}
            </ul>
          </dd>

          <dt className="mt-4 text-md font-semibold">
            Price
          </dt>
          <dd className="text-blue-500 text-bold text-4xl ml-8">
            {product.price !== '0.0'
              ? `${product.price_sign !== null ? `${product.price_sign} ` : ''}${product.price}`
              : 'Sold-Out'
            }
          </dd>

          <dt className="mt-4 text-md font-semibold">
            Rating
          </dt>

          <dd>{product.rating || 'No rated yet'}</dd>
        </dl>
      </div>
    </PageTemplate>
  )
}

ProductDetail.propTypes = propTypes
ProductDetail.defaultProps = defaultProps

export default ProductDetail
