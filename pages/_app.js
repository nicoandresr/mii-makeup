import '../styles/globals.css'
import PropTypes from 'prop-types'

const propTypes = {
  Component: PropTypes.element,
  pageProps: PropTypes.object,
}

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

MyApp.propTypes = propTypes

export default MyApp
