import PropTypes from 'prop-types'

import PageTemplate from '../components/page-template'
import ProductList from '../components/product-list'
import host from '../lib/host'

const propTypes = {
  products: PropTypes.array
}

const defaultProps = {
  products: []
}

export async function getStaticProps() {
  const response = await fetch(`${host()}/products.json`)
  const products = await response.json()
  return {
    props: { products: products.slice(0, 20) }
  }
}

function Home({ products }) {
  return (
    <PageTemplate name="Home">
      <h1 className="text-4xl p-4">
        Beaty & Personal Care
      </h1>

      <ProductList products={products} />
    </PageTemplate>
  )
}

Home.propTypes = propTypes
Home.defaultProps = defaultProps

export default Home
