This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

# Screenshots

## Home
![Home](/uploads/f656ee4bc836a708b401ace2e971bf01/Home.png)

## PDP
![ProductDetailPage](/uploads/fc985197e4a76d76fd013e71f4532f0b/ProductDetailPage.png)

## Search 
![SearchPage](/uploads/0e92aeb7597e3c5cd5b45ad4e255dc17/SearchPage.png)

## Getting Started

First, install

```bash
npm install
```

Then run the development server:

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.js`. The page auto-updates as you edit the file.

[API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on:
- [http://localhost:3000/api/product/list](http://localhost:3000/api/product/list).
- [http://localhost:3000/api/product/detail/[id]](http://localhost:3000/api/product/detail/[id]).
- [http://localhost:3000/api/product/search?name=''&type=''&brand=''](http://localhost:3000/api/product/search?name=''&type=''&brand='').
