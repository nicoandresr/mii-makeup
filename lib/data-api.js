import cache from './cache-api'

const defaultApiURL = 'https://makeup-api.herokuapp.com/api/v1'

const API_URL = process.env.API_URL || defaultApiURL

async function dataApi() {
  let productsCache
  let products

  try {
    productsCache = cache.entry({ name: 'products' })
  } catch (e) {
    console.error(`Unable to create cache entry`, e)
  }

  try {
    if (productsCache.valid()) {
      products = JSON.parse(productsCache.read())
    } else {
      const response = await fetch(`${API_URL}/products.json`)
      products = await response.json()
      productsCache.write(JSON.stringify(products))
    }
  } catch (error) {
    console.error(`API error: unable to fetch product list`, error)
  }

  return {
    products,
    productById(id) {
      let [first] = products.filter(product => product.id === id)
      return first
    }
  }
}

export default dataApi
