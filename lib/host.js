function host() {
  if (!process.env.API_URL) {
    return 'https://makeup-api.herokuapp.com/api/v1'
  }

  return process.env.API_URL
}

export default host
