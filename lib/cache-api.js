import fs from 'fs'
import path from 'path'

const ENCODING = 'utf8'

const defaults = {
  name: 'fs',
}

const cacheApi = {
  entry(options = defaults) {
    const { name } = options
    const file = path.join('.', `cache-${name}.txt`)

    return {
      valid() {
        if (!fs.existsSync(file)) return false

        // Here we can improve the cache invalidate it 
        // when reach's the max age
        // in this moment if exits returns true

        return true
      },
      read() {
        if (!fs.existsSync(file)) return ''

        try {
          const value = fs.readFileSync(file, ENCODING)
          console.info(`cache-api success: on read entry `, file)
          return value || ''
        } catch (e) {
          console.error(`cache-api error on read: `, file, e)
          return false
        }
      },
      write(value) {
        try {
          fs.writeFileSync(file, value, ENCODING)
          console.info(`cache-api success: on write entry `, file)
        } catch (e) {
          console.error(`cache-api error on write: `, file, e)
        }
      },
    }
  }
}

export default cacheApi
